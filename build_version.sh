#!/bin/bash
OUTPUTFILE=$2
BUILDNUMBER=$1

function out {
  if [ -n "$1" ];then
     echo "$2" >> "$1"
  else
     echo "$2"
  fi
}

version_regex='([0-9]+)\.([0-9]+)\.?([0-9]*)-([0-9]+)-g([0-9|a-z]+)'
git_string=$(git describe --tags --long)

if [[ $git_string =~ $version_regex ]]; then
    major_version="${BASH_REMATCH[1]}"
    minor_version="${BASH_REMATCH[2]}"
    patch_version="${BASH_REMATCH[3]}"
    commits_ahead="${BASH_REMATCH[4]}"
    commit_hash="${BASH_REMATCH[5]}"
else
    echo "Error: git describe did not output a valid version string. Unable to update git_version.cpp" >&2
    exit 1
fi

if [ -z "$BUILDNUMBER" ];then
    BUILDNUMBER=$BUILD_NUMBER
fi
if [ -z "$OUTPUTFILE" ];then
    OUTPUTFILE=$ARCHIVE_DIR/build.properties
fi

version_num="${major_version}.${minor_version}.${patch_version}"
version_num_plus_build="${major_version}.${minor_version}.${patch_version}-${BUILDNUMBER}"
version_num_plus_commits_ahead="${major_version}.${minor_version}.${patch_version}+${commits_ahead}"

APP_VERSION="$version_num_plus_build"
APP_VERSION_SHORT="$version_num"
APP_VERSION_LONG="$version_num_plus_build-${commit_hash}"
APP_VERSION_COMMITS_AHEAD="$version_num_plus_commits_ahead"

out "$OUTPUTFILE" "APP_VERSION=$APP_VERSION"
out "$OUTPUTFILE" "APP_VERSION_SHORT=$APP_VERSION_SHORT"
out "$OUTPUTFILE" "APP_VERSION_LONG=$APP_VERSION_LONG"
out "$OUTPUTFILE" "APP_VERSION_COMMITS_AHEAD=$APP_VERSION_COMMITS_AHEAD"

if [ -f "./chart/${IMAGE_NAME}/Chart.yaml" ];then
  echo "Setting AppVersion to ${APP_VERSION}, modifying Chart.yaml"
  sed -i s/%%APP_VERSION%%/${APP_VERSION}/ ./chart/${IMAGE_NAME}/Chart.yaml
fi
